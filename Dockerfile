FROM git as cloner

WORKDIR /SRV

COPY . .



RUN git clone https://github.com/tgalal/yowsup/wiki/Sample-Application

FROM python:3 as runner

COPY --from=cloner ./yowsup .

RUN "cd yowsup && python3 setup.py && rm -rf yowsup"

RUN "pip3 -r requirements.txt"

CMD ["python", "./run.py"]