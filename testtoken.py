#! /usr/bin/env python3
import base64

"""lst = [
    "1@+/NrGS7jfNFygRvIlC8G/4iQLvWXIw8HBP3s3Z1kPigBp5onuFj/+AQJsqDSmrBZzx96LrPbN/8PFQ==",
    "oRHkdShvcT4PpvATbEvEHR5xWplXkF7YMRFoEQDGhMc=",
    "6Pk8s1QIobc53n7pymDuwoyg+y/OfXSVeH5iCtIoHEY=",
    
]

for i in lst:
    decoded = b64decode(i)
    len_ = len([_ for _ in decoded])
    print(f"{len_=} {len_ == 64} {decoded=}")"""

"""

tmp = base64.b64encode(b"picasso2005   :" + base64.b64decode("kUYtUaDGC++D98P9Cj4ndb3KMIWY12/gctkpZQFzODg="))
print(tmp)
print(len([_ for _ in tmp]))

"""

import html
v = html.unescape("""<string name="media_conn">{&quot;auth_token&quot;:&quot;AWQndStJlOTOM87vjWaNswBQ-FO1Rzifpy-9iX9gBu3Jhcte3yUbt4KNhSM&quot;,&quot;conn_ttl&quot;:300,&quot;auth_ttl&quot;:21600,&quot;max_buckets&quot;:12,&quot;hosts&quot;:[{&quot;hostname&quot;:&quot;media-cdg2-1.cdn.whatsapp.net&quot;,&quot;ip4&quot;:&quot;179.60.192.51&quot;,&quot;ip6&quot;:&quot;2a03:2880:f21f:c5:face:b00c:0:167&quot;,&quot;fallback_hostname&quot;:&quot;media-lcy1-2.cdn.whatsapp.net&quot;,&quot;fallback_ip4&quot;:&quot;157.240.232.60&quot;,&quot;fallback_ip6&quot;:&quot;2a03:2880:f264:1c1:face:b00c:0:167&quot;,&quot;upload&quot;:[],&quot;download_buckets&quot;:[&quot;100&quot;,&quot;102&quot;,&quot;104&quot;,&quot;107&quot;],&quot;type&quot;:&quot;primary&quot;,&quot;force_ip&quot;:false},{&quot;hostname&quot;:&quot;media-cdt1-1.cdn.whatsapp.net&quot;,&quot;ip4&quot;:&quot;157.240.21.52&quot;,&quot;ip6&quot;:&quot;2a03:2880:f230:c5:face:b00c:0:167&quot;,&quot;fallback_hostname&quot;:&quot;media-lcy1-2.cdn.whatsapp.net&quot;,&quot;fallback_ip4&quot;:&quot;157.240.232.60&quot;,&quot;fallback_ip6&quot;:&quot;2a03:2880:f264:1c1:face:b00c:0:167&quot;,&quot;upload&quot;:[],&quot;download_buckets&quot;:[&quot;101&quot;,&quot;103&quot;,&quot;105&quot;,&quot;106&quot;],&quot;type&quot;:&quot;primary&quot;,&quot;force_ip&quot;:false},{&quot;hostname&quot;:&quot;media-cdt1-1.cdn.whatsapp.net&quot;,&quot;ip4&quot;:&quot;157.240.21.52&quot;,&quot;ip6&quot;:&quot;2a03:2880:f230:c5:face:b00c:0:167&quot;,&quot;fallback_hostname&quot;:&quot;media-cdg2-1.cdn.whatsapp.net&quot;,&quot;fallback_ip4&quot;:&quot;179.60.192.51&quot;,&quot;fallback_ip6&quot;:&quot;2a03:2880:f21f:c5:face:b00c:0:167&quot;,&quot;download_buckets&quot;:[&quot;0&quot;],&quot;type&quot;:&quot;primary&quot;,&quot;force_ip&quot;:false},{&quot;hostname&quot;:&quot;media-cdg2-1.cdn.whatsapp.net&quot;,&quot;ip4&quot;:&quot;179.60.192.51&quot;,&quot;ip6&quot;:&quot;2a03:2880:f21f:c5:face:b00c:0:167&quot;,&quot;upload&quot;:[],&quot;download_buckets&quot;:[&quot;111&quot;,&quot;108&quot;],&quot;type&quot;:&quot;primary&quot;,&quot;force_ip&quot;:false},{&quot;hostname&quot;:&quot;mmg.whatsapp.net&quot;,&quot;fallback_hostname&quot;:&quot;mmg-fallback.whatsapp.net&quot;,&quot;type&quot;:&quot;fallback&quot;,&quot;force_ip&quot;:false}],&quot;send_time_abs_ms&quot;:1647127073888,&quot;last_id&quot;:&quot;134010059&quot;,&quot;is_new&quot;:true,&quot;max_autodownload_retry&quot;:3,&quot;max_manual_retry&quot;:3}</string>                                 </map>""")
print(v)