#! /bin/python3

from yowsup.stacks import  YowStackBuilder
from layer import EchoLayer
from yowsup.layers import YowLayerEvent
from yowsup.layers.network import YowNetworkLayer
from yowsup.profile.profile import YowProfile
from yowsup.config.manager import Config
from yowsup.layers.noise.layer import KeyPair
from consonance.structs.publickey import PublicKey
import base64


# Decoded +double escaped keystore client_static_moncul_enc  '[2,"aOOUH6xaGqRuoYyGBEPtyGUfD554+J\\/cvY4T5Yob7rdp1aA9Cb3cL0crUa\\/3fWvqF\\/Ih3Vv8Hrhz7hF5vyX+fA","+ROB5c5qTDs2zjk1mt2eCw","o3bW3g","qpWtymeR6yZFzzTUuDZnNg"]'


config = Config (
    phone="33768631204", # U_Bren phone
    pushname="U_Bren",
    cc="33",
    mcc="208", # France's MCC
    mnc="15", # Free
    client_static_keypair=KeyPair.from_bytes(base64.b64decode("+KJ7qpfv+PegXrd+YD4hO4g0HebBPm20He/DktJ2NUEcDRKxHThj1m3rotoeeAn10R/cNA4Ti+bcUWv35SKpcQ==")), # Extracted from /data/data/com.whatsapp/shared_pref/keystore.xml "client_static_keypair_pwd_enc", html-unencoded, and then decrypted using WAPI's decoder
    server_static_public = PublicKey(base64.b64decode("qJWvSttNopqgQ2CgXYTc4jmSUKWd1Rv2QTMbQyYpKwY=")), # Extracted from /data/data/com.whatsapp/shared_pref/keystore.xml "server_static_public"
    edge_routing_info="CAkICA==", # Extracted from /data/data/com.whatsapp/shared_pref/com.whatsapp_preferences_light.xml
    fdid="522e7c76-2211-4ce5-ac55-55045de5347b" # Extracted from /data/data/com.whatsapp/shared_pref/com.whatsapp_preferences_light.xml

    )

profile = YowProfile("picasso2005", config=config)

if __name__==  "__main__":
    stackBuilder = YowStackBuilder()

    stack = stackBuilder\
        .pushDefaultLayers()\
        .push(EchoLayer)\
        .build()
    
    #stack.setCredentials(credentials)
    stack.setProfile(profile)
    stack.broadcastEvent(YowLayerEvent(YowNetworkLayer.EVENT_STATE_CONNECT))   #sending the connect signal
    stack.loop() #this is the program mainloop